package nl.api.dc_add;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class test {
    @GetMapping(
        value = "/test",
        produces = MediaType.IMAGE_JPEG_VALUE
      )
      public @ResponseBody byte[] getImageWithMediaType() throws IOException {
        File directoryPath = new File("dc_add/src/main/resources/static");
        File files[] = directoryPath.listFiles();
        int random_index = new Random().nextInt(files.length);
        String name = files[random_index].getName();
          InputStream in = getClass()
            .getResourceAsStream("/static/" + name);
          return IOUtils.toByteArray(in);
      }
}
