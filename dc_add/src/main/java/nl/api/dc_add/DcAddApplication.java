package nl.api.dc_add;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DcAddApplication {

	public static void main(String[] args) {
		SpringApplication.run(DcAddApplication.class, args);
	}

}
